//
//  PlayerController.m
//  Vitamio-Demo
//
//  Created by erlz nuo(nuoerlz@gmail.com) on 7/8/13.
//  Copyright (c) 2013 yixia. All rights reserved.
//

#import "PlayerController.h"


@interface PlayerController()
{
    VMediaPlayer       *mMPayer;
    UIView             *videoView;
}

@property (nonatomic, copy) NSURL  *videoURL;

@end



@implementation PlayerController
#pragma mark - Life Cycle


- (void) setVideoViewOBJ:(UIView*)view
{
    self->videoView = view;
}

- (void) startVideoStreamOBJ:(NSString*)stringVideoUrl
{
    mMPayer = [VMediaPlayer sharedInstance];
    [mMPayer setupPlayerWithCarrierView:self->videoView withDelegate:self];
    _videoURL = [NSURL URLWithString:stringVideoUrl];
    [mMPayer setDataSource:_videoURL header:nil];
    [mMPayer prepareAsync];
}

- (void)dealloc
{
   [self unSetupObservers];
    [mMPayer unSetupPlayer];
    //[mMPayer reset];
    //[_videoURL release];
    

}

- (void)stop
{
    
    [self unSetupObservers];
    //[mMPayer unSetupPlayer];
    [mMPayer reset];


}

- (void)pause
{
    [mMPayer pause];
}

- (void) play
{
    [mMPayer start];
}

- (void)unSetupObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


// This protocol method will be called when player did prepared, so we can
// call -start in here to start playing.
- (void)mediaPlayer:(VMediaPlayer *)player didPrepared:(id)arg
{
    [player start];
}
// This protocol method will be called when playback complete, so we can
// do something here, e.g. reset player.
- (void)mediaPlayer:(VMediaPlayer *)player playbackComplete:(id)arg
{
    [player reset];
}
// If an error occur, this protocol method will be triggered.
- (void)mediaPlayer:(VMediaPlayer *)player error:(id)arg
{
    NSLog(@"NAL 1RRE &&&& VMediaPlayer Error: %@", arg);
}

@end