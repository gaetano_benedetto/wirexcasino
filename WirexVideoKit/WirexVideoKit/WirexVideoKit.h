//
//  WirexVideoKit.h
//  WirexVideoKit
//
//  Created by Gaetano Benedetto on 20/07/15.
//  Copyright © 2015 Wirex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WirexVideoKit/PlayerController.h>
#import <WirexVideoKit/PlayerControllerDelegate.h>


//! Project version number for WirexVideoKit.
FOUNDATION_EXPORT double WirexVideoKitVersionNumber;

//! Project version string for WirexVideoKit.
FOUNDATION_EXPORT const unsigned char WirexVideoKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WirexVideoKit/PublicHeader.h>


