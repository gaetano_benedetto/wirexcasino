//
//  ViewController.swift
//  testVitamioSwift
//
//  Created by Gaetano Benedetto on 20/07/15.
//  Copyright © 2015 Wirex. All rights reserved.
//

import UIKit


public class VideoViewController: NSObject{

    public var player: PlayerController
    
    public override init() {
          self.player = PlayerController()
    }
    
    public func setVideoView(view: UIView)
    {
        print("setVideoView")
        self.player.setVideoViewOBJ(view);
    }
    
    public func startVideoStream(stringVideoUrl: String)
    {
        print("startVideoStream")
        self.player.startVideoStreamOBJ(stringVideoUrl)
    }
    
    public func stop()
    {
        print("stopVideoStream")
        self.player.stop()
    }
    
    public func play()
    {
        print("play")
        self.player.play()
    }
    public func pause()
    {
        print("pause")
        self.player.pause()
    }
    

}

