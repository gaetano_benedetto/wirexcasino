//
//  WebViewDelegate.swift
//  WirexGameKit
//
//  Created by Gaetano Benedetto on 31/08/15.
//  Copyright (c) 2015 Wirex. All rights reserved.
//

import Foundation
import UIKit

protocol JavascriptObjectDelegate {
    func call( action: String, callback: String, data: String )
}

class WebViewDelegate: NSObject, UIWebViewDelegate {
    
    /**
    *  set window.location to string with a blank will not troggle the following function
    */
    var jod: JavascriptObjectDelegate?
    
    init(delegate: JavascriptObjectDelegate) {
        jod = delegate
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        NSLog("Processing javascript call")
        
        let jsPrefix: String = "js://"
        let jsPrefixLen: Int = count(jsPrefix)
        
        do {
            // get url string
            let urlString: String? = request.URL!.absoluteString
            if urlString == nil { break }
            
            // test if a prefix exists
            if !urlString!.hasPrefix(jsPrefix) { break }
            let requestString: String = urlString!.substringFromIndex(advance(urlString!.startIndex, jsPrefixLen)) as String!
            
            var requests:Array<String> = requestString.componentsSeparatedByString("$");
            if requests.count != 3 { break }
            
            for var i=0; i<requests.count; i++ {
                requests[i] = requests[i].stringByReplacingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            }
            
            jod!.call(requests[0], callback: requests[1], data: requests[2])
            
            return false
            
        } while false
        
        return true
    }

    
}