//
//  HttpRequest.swift
//  WirexGameKit
//
//  Created by Gaetano Benedetto on 27/08/15.
//  Copyright © 2015 Wirex. All rights reserved.

import Foundation

typealias JSONDictionary = Dictionary<String, AnyObject>
typealias JSONArray = Array<AnyObject>
//var dict = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("Config", ofType: "plist")!)

protocol HttpRequestControllerProtocol {
    func didReceiveHttpRequestResults(results: NSDictionary?)
}

class HttpRequest: NSObject, NSURLConnectionDataDelegate {
    
    var delegate: HttpRequestControllerProtocol?
    
    init(delegate: HttpRequestControllerProtocol?) {
        self.delegate = delegate
    }
    
    typealias HttpRequestCallback = ((NSDictionary?, NSString?) -> ())
    
    func getData(url: NSString, callback: HttpRequestCallback){
        self.makeHTTPGetRequest((url as String)){
            (data, error) -> Void in
            if (error == nil){
                self.delegate?.didReceiveHttpRequestResults(data)
            }
        }
    }
    
    func makeHTTPGetRequest(url: NSString, callback: HttpRequestCallback){
        println("path \(url)")
        var urlObject = NSURL(string: url as String)
        var request = NSMutableURLRequest(URL: urlObject!)
        request = self.buildRequestHeaders(request)
        request.HTTPMethod = "GET"
        
        httpRequest(request){
            (data, error) -> Void in
            callback(data, error)
        }
    }
    
    func httpRequest(request: NSURLRequest!, callback: HttpRequestCallback) {
        var session = NSURLSession.sharedSession()
        var task = session.dataTaskWithRequest(request){
            (data, response, error) -> Void in
            if error != nil {
                callback(nil, error.localizedDescription)
            } else {
                let responseString = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("responseString = \(responseString!)")
                var jsonResult: AnyObject? = NSJSONSerialization.JSONObjectWithData(data, options:    NSJSONReadingOptions.MutableContainers, error: nil)
                if let nsDictionaryObject = jsonResult as? NSDictionary {
                    if (jsonResult != nil) {
                        callback(nsDictionaryObject, nil)
                    }else{
                        callback(nil, nil)
                    }
                }
            }
        }
        task.resume()
    }
    
    func buildRequestHeaders(request: NSMutableURLRequest) -> NSMutableURLRequest{
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        return request
    }
    
    /*func getPOSTData(username: String , password: String){
        var request = NSMutableURLRequest(URL: NSURL(string: "http://www.portomasogaming-stage.com/user/loginCommon.jss")!)
        request.HTTPMethod = "POST"
        let postString = "operation=login&username=\(username)&password=\(password)";
        
        
        request.HTTPBody = postString.dataUsingEncoding(NSUTF8StringEncoding)
        httpRequest(request){
            (data, error) -> Void in
            if (error == nil){
                self.delegate?.didReceiveHttpRequestResults(data)
            }
        }
        
    }
*/
    
}
