//
//  WirexWebKitController.swift
//  WirexGameKit
//
//  Created by Gaetano Benedetto on 31/08/15.
//  Copyright (c) 2015 Wirex. All rights reserved.
//

import UIKit
import WebKit


class WirexWebKitController: UIViewController, WKScriptMessageHandler {
    var viewWeb: WKWebView?
    @IBOutlet var containerView : UIView! = nil
    
    var stringWebUrl : String = "http://gamingportal.grngmstage.com/smartcasino/html/rouletteTestLoader2.jsp?langtag=en&previousContextId=6946799&businessOperatorId=196568&gameID=mOracle42&familyGameId=42&uiSpecId=15&lobbyURL=http://m.portomasogaming-stage.com/wx/casino/category/roulette/8/&channel=1"
    
    override  func loadView() {
        super.loadView()
        
        var contentController = WKUserContentController();
        var userScript = WKUserScript(
            source: "redHeader()",
            injectionTime: WKUserScriptInjectionTime.AtDocumentEnd,
            forMainFrameOnly: true
        )
        contentController.addUserScript(userScript)
        contentController.addScriptMessageHandler(
            self,
            name: "callbackHandler"
        )
        
        var config = WKWebViewConfiguration()
        config.userContentController = contentController
        
        self.viewWeb = WKWebView(
            frame: self.containerView.bounds,
            configuration: config
        )
        self.viewWeb!.opaque = false
        self.viewWeb!.backgroundColor = UIColor.clearColor()
        self.view = self.viewWeb!
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func loadRequest()
    {
        
        var url = NSURL(string:stringWebUrl)
        var req = NSURLRequest(URL:url!)
        self.viewWeb!.loadRequest(req)
        
    }
    
    func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        if(message.name == "callbackHandler") {
            println("JavaScript is sending a message \(message.body)")
        }
    }
}