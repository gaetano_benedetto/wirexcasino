//
//  WirexWebController.swift
//  WirexGameKit
//
//  Created by Gaetano Benedetto on 28/08/15.
//  Copyright (c) 2015 Wirex. All rights reserved.
//

import UIKit
import WebKit


 class WirexWebController: UIViewController {

    @IBOutlet var viewWeb: UIWebView!
    
    var appJavascriptDelegate: AppJavascriptDelegate?
    var webViewDelegate: WebViewDelegate?

    var stringWebUrl : String = "http://gamingportal.grngmstage.com/smartcasino/html/rouletteTestLoader2.jsp?langtag=en&previousContextId=6946799&businessOperatorId=196568&gameID=mOracle42&familyGameId=42&uiSpecId=15&lobbyURL=http://m.portomasogaming-stage.com/wx/casino/category/roulette/8/&channel=1"
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func loadRequest()
    {
        self.viewWeb!.opaque = false
        self.viewWeb!.backgroundColor = UIColor.clearColor()
        self.view!.opaque = false
        self.view!.backgroundColor = UIColor.clearColor()
        var url = NSURL(string:stringWebUrl)
        var req = NSURLRequest(URL:url!)
        self.viewWeb!.loadRequest(req)
        
        self.viewWeb.scrollView.bounces = false
        
        /*appJavascriptDelegate = AppJavascriptDelegate(wv: self.viewWeb!)
        webViewDelegate = WebViewDelegate(delegate: appJavascriptDelegate!)
        self.viewWeb.delegate = webViewDelegate*/

    }
    
    func stop()
    {
        appJavascriptDelegate = nil
        webViewDelegate = nil
        self.viewWeb.delegate = nil
    }
    
    
}