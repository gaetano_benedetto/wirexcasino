//
//  WirexGameController.swift
//  WirexGameKit
//
//  Created by Gaetano Benedetto on 27/08/15.
//  Copyright © 2015 Wirex. All rights reserved.
//

import UIKit
import WebKit


public class WirexGameController: UIViewController, TableGameSpecProtocol{
    @IBOutlet var viewVideo: UIView!

   
    var videoController:WirexVideoController?
    var webController:WirexWebController?
    
    var extWindow : UIWindow = UIWindow();
    var extScreen : UIScreen = UIScreen();

    var stringVideoUrl: String = ""
     var stringWebUrl : String = ""
    
    var tableGameSpec : TableGameSpec = TableGameSpec()

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillEnterForeground:", name: UIApplicationWillEnterForegroundNotification, object: nil)

         NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillEnterBackground:", name: UIApplicationDidEnterBackgroundNotification, object: nil)
       
        loadTableSpec();
        
    }
    
    
    
    func loadTableSpec()
    {
        if(self.stringWebUrl == ""){
            println("self.stringWebUrl is empty")
        }
        else
        {

            var url : NSURL = NSURL(string: self.stringWebUrl)!
            
            var params : NSMutableDictionary = NSMutableDictionary()
            var paramList =  url.query!.componentsSeparatedByCharactersInSet(NSCharacterSet (charactersInString: "&"))
            
            for param in paramList
            {
                var elts = param.componentsSeparatedByString("=");
                if(elts.count == 2)
                {
                    params.setObject(elts[1] as String, forKey: elts[0])
                }
                
            }
            
            var platformDomain = url.host!
            
            self.tableGameSpec.delegate = self
            
            if((params.objectForKey("uiSpecId")) != nil)
            {
                var uiSpecId = params.objectForKey("uiSpecId") as! String
                println("uiSpecId \(uiSpecId)")
                self.tableGameSpec.loadRemoteTableGameSpec(platformDomain, uiSpecId: uiSpecId)
            }
            else if((params.objectForKey("operator")) != nil && (params.objectForKey("game")) != nil)
            {
                var externalOperator = params.objectForKey("operator") as! String
                var game = params.objectForKey("game") as! String
                
                self.tableGameSpec.loadRemoteTableGameSpec(platformDomain, externalOperator: externalOperator, game: game)
            }

        }

    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    public  func stop() {
        self.videoController!.stop();
        self.webController?.stop();
        self.tableGameSpec.delegate = nil
    }
    
    public  func initializeWithWebUrl(webUrl :String) {
 
        self.stringWebUrl = webUrl
    }
    
    func didTableGameSpecLoaded()
    {
        stringVideoUrl =  self.tableGameSpec.videoStreamUrl + "/" +  self.tableGameSpec.videoStreamUrlFileId
        println("stringVideoUrl \(stringVideoUrl)")
        
        let orientation = UIApplication.sharedApplication().statusBarOrientation
        self.videoLoader(orientation.isLandscape)
        
        self.webLoader()

    }

    public override func viewWillTransitionToSize(size: CGSize, withTransitionCoordinator coordinator: UIViewControllerTransitionCoordinator)
    {
        var screens = UIScreen.screens()
        let screenCount = screens.count
        if (screenCount == 1)
        {
            if (self.videoController != nil)
            {
                self.videoController!.stop();
                self.videoController = nil
           
                var landscape = false
                if(size.height < size.width)
                {
                    landscape = true
                }
                
                self.videoLoader(landscape)
            }
            if (self.webController != nil)
            {
                self.webController!.view.removeFromSuperview()
                self.view.addSubview(self.webController!.view)
            }
       
        }
       
    }
    
    func videoLoader(landscape: Bool)
    {
        let frameworkBundle = NSBundle(identifier: "com.wirex.casino.WirexGameKit")
        let storyboard : UIStoryboard = UIStoryboard(name: "WirexGame", bundle: frameworkBundle)
        
        
        var screens = UIScreen.screens()
        let screenCount = screens.count
        if (screenCount > 1)
        {
            self.videoController = storyboard.instantiateViewControllerWithIdentifier("WirexVideoControllerLandScape") as? WirexVideoController
            self.videoController!.stringVideoUrl = stringVideoUrl
            
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "screenDidChange", name: UIScreenDidConnectNotification, object: nil)
            NSNotificationCenter.defaultCenter().addObserver(self, selector: "screenDidChange", name: UIScreenDidDisconnectNotification, object: nil)
            
            // 2. If an external screen is available, get the screen object and look at the values in its availableModes
            // property. This property contains the configurations supported by the screen.
            
            // Select first external screen
            self.extScreen = screens[1] as! UIScreen; //index 0 is your iPhone/iPad
            let availableModes = self.extScreen.availableModes;
            
            // 3. Select the UIScreenMode object corresponding to the desired resolution and assign it to the currentMode
            // property of the screen object.
            
            // Select the highest resolution in this sample
            let selectedRow = availableModes.count - 1;
            self.extScreen.currentMode = availableModes[selectedRow] as? UIScreenMode;
            
            // Set a proper overscanCompensation mode
            self.extScreen.overscanCompensation = UIScreenOverscanCompensation.InsetApplicationFrame;
            
            self.extWindow = UIWindow(frame: self.extScreen.bounds)
            
            // 5. Assign the screen object to the screen property of your new window.
            self.extWindow.screen = self.extScreen;
            
            // 6. Configure the window (by adding views or setting up your OpenGL ES rendering context).
            
            // Resize the GL view to fit the external screen
            self.videoController!.view.frame = self.extWindow.frame;
            
            
            // Add the GL view
            self.extWindow.addSubview(self.videoController!.view)
            
            // 7. Show the window.
            self.extWindow.makeKeyAndVisible()
            
        }
        else
        {
            if(landscape)
            {
                self.videoController = storyboard.instantiateViewControllerWithIdentifier("WirexVideoControllerLandScape") as? WirexVideoController
            }
            else
            {
                self.videoController = storyboard.instantiateViewControllerWithIdentifier("WirexVideoController") as? WirexVideoController
            }
            self.view.addSubview(self.videoController!.view)
            self.videoController!.stringVideoUrl = stringVideoUrl
            
        }
        
        self.videoController!.start();
    }
    
    func webLoader()
    {
        let frameworkBundle = NSBundle(identifier: "com.wirex.casino.WirexGameKit")
        let storyboard : UIStoryboard = UIStoryboard(name: "WirexGame", bundle: frameworkBundle)
        self.webController = storyboard.instantiateViewControllerWithIdentifier("WirexWebController") as? WirexWebController
        
        self.webController!.stringWebUrl = stringWebUrl
        
        self.view.addSubview(self.webController!.view)
        self.webController!.loadRequest()
    }
    
    func applicationWillEnterForeground(application: UIApplication!) {

        /*var screens = UIScreen.screens()
        let screenCount = screens.count
        if (screenCount == 1)
        {
            if (self.videoController != nil)
            {
                
                let orientation = UIApplication.sharedApplication().statusBarOrientation
                self.videoLoader(orientation.isLandscape)
            }
            
            if (self.webController != nil)
            {
                self.webController!.view.removeFromSuperview()
                self.view.addSubview(self.webController!.view)
            }
            
        }
        */
        self.videoController!.play()
    }
    
    func applicationWillEnterBackground(application: UIApplication!) {
       self.videoController!.pause();
    }
    
    
}
