//
//  TableGameSpec.swift
//  WirexGameKit
//
//  Created by Gaetano Benedetto on 04/09/15.
//  Copyright (c) 2015 Wirex. All rights reserved.
//

import UIKit

protocol TableGameSpecProtocol {
    func didTableGameSpecLoaded()
}

class TableGameSpec :NSObject, HttpRequestControllerProtocol
{
    lazy var httpRequest: HttpRequest = HttpRequest(delegate: self)
    let PATH_TABLEGAMESPEC = "/smartcasino/util/tableGameSpec.jsp"
    var delegate: TableGameSpecProtocol?
    var loaderHtml = "";
    var videoStreamUrl = "";
    var videoStreamUrlFileId = "";


    
    func didReceiveHttpRequestResults(results: NSDictionary?) {
        var resultDict = results!
        
        dispatch_async(dispatch_get_main_queue()) {
           // println("resultDict : \(resultDict)")
            self.loaderHtml = ((resultDict.objectForKey("SGUITableGameSpec"))?.objectForKey("loaderHtml")) as! String
            self.videoStreamUrl = ((resultDict.objectForKey("SGUITableGameSpec"))?.objectForKey("videoStreamUrl")) as! String
            self.videoStreamUrlFileId = ((resultDict.objectForKey("SGUITableGameSpec"))?.objectForKey("videoStreamUrlFileId")) as! String
            
            println("self.loaderHtml : \(self.loaderHtml)")
            println("self.videoStreamUrl : \(self.videoStreamUrl)")
            println("self.videoStreamUrlFileId : \(self.videoStreamUrlFileId)")
            
            self.delegate?.didTableGameSpecLoaded()
        }
    }
    
    func loadRemoteTableGameSpec(platformDomain :String, externalOperator :String, game :String)
    {
        let stringSpecUrl = "http://" + platformDomain + PATH_TABLEGAMESPEC + "?op=" + externalOperator + "&g=" + game
        httpRequest.getData(stringSpecUrl){(data, error) -> Void in}
    }
    
    func loadRemoteTableGameSpec(platformDomain :String, uiSpecId :String)
    {
        let stringSpecUrl = "http://" + platformDomain + PATH_TABLEGAMESPEC + "?uisId=" + uiSpecId
        httpRequest.getData(stringSpecUrl){(data, error) -> Void in}
    }
    
}

