//
//  WirexGameKit.h
//  WirexGameKit
//
//  Created by Gaetano Benedetto on 27/08/15.
//  Copyright © 2015 Wirex. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WirexGameKit.
FOUNDATION_EXPORT double WirexGameKitVersionNumber;

//! Project version string for WirexGameKit.
FOUNDATION_EXPORT const unsigned char WirexGameKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WirexGameKit/PublicHeader.h>


