//
//  WirexVideoController.swift
//  WirexGameKit
//
//  Created by Gaetano Benedetto on 27/08/15.
//  Copyright © 2015 Wirex. All rights reserved.
//

import UIKit
import WirexVideoKit

class WirexVideoController: UIViewController {
    @IBOutlet var viewVideo: UIView!
    
    var stringVideoUrl: String = "rtmp://tumasgmlivefs.fplive.net/tumasgmlive-live/streamSMALLORACLERCR2"
    var videoController: VideoViewController = VideoViewController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func stop() {
        videoController.stop();
    }
    
    func start() {
        videoController.setVideoView(self.viewVideo)
        videoController.startVideoStream(self.stringVideoUrl)
    }
    
    func play() {
        videoController.play();
    }
    
    func pause() {
        videoController.pause()
    }
    
    
}
