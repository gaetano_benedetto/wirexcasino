//
//  ViewController.swift
//  WirexGameDemo
//
//  Created by Gaetano Benedetto on 27/08/15.
//  Copyright © 2015 Wirex. All rights reserved.
//

import UIKit
import WirexGameKit

class GameController: UIViewController{
    
    //var vc : WirexGameController = WirexGameController()
    var vc : WirexGameController?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func loadRoulette(sender: UIButton) {
        let frameworkBundle = NSBundle(identifier: "com.wirex.casino.WirexGameKit")
        let storyboard : UIStoryboard = UIStoryboard(name: "WirexGame", bundle: frameworkBundle)
        self.vc = storyboard.instantiateViewControllerWithIdentifier("WirexGame") as? WirexGameController
        
        // let stringWebUrl = "https://gamingportal.grngmstage.com/smartcasino/ext/techssonLoadGame?operator=krooncasino&lang=nl&currency=EUR&client=html&game=vipBlackjackOracle1&session=e0f67bcd-f4c4-4574-8c6b-d081397219ae"
        
        let stringWebUrl = "http://gamingportal.grngmstage.com/smartcasino/html/rouletteTestLoader.jsp?langtag=en&previousContextId=6946799&businessOperatorId=196568&gameID=mOracle42&familyGameId=42&uiSpecId=32&lobbyURL=http://m.portomasogaming-stage.com/wx/casino/category/roulette/8/&channel=1"

        self.vc!.initializeWithWebUrl(stringWebUrl)

        
        let navigationController = UINavigationController(rootViewController: self.vc!)
        self.presentViewController(navigationController, animated: true, completion: nil)
        self.vc!.navigationItem.hidesBackButton = true
        let newBackButton = UIBarButtonItem(title: "Back", style: UIBarButtonItemStyle.Plain, target: self, action: "back:")
        self.vc!.navigationItem.leftBarButtonItem = newBackButton;
        
    }
    
    
    func back(sender: UIBarButtonItem) {
        self.vc!.stop()
        self.dismissViewControllerAnimated(false, completion: nil)
        self.vc = nil
    }
    

    
}

