//
//  ViewController.swift
//  WirexVideoDemo
//
//  Created by Gaetano Benedetto on 20/07/15.
//  Copyright © 2015 Wirex. All rights reserved.
//

import UIKit
import WirexVideoKit


class VDViewController: UIViewController {
    @IBOutlet var carrier: UIView!
    var stringVideoUrl: String = "rtmp://tumasgmlivefs.fplive.net/tumasgmlive-live/streamSMALLORACLERCR"
    var videoController: VideoViewController = VideoViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        videoController.setVideoView(self.carrier)
        videoController.startVideoStream(self.stringVideoUrl)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

